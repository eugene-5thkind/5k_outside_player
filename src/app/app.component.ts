import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { take, debounceTime, catchError } from 'rxjs/operators';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'fifth-kind-outside-player';
  vars = [];
  ready = new BehaviorSubject(false);
  message = new BehaviorSubject('Loading...');

  constructor(private route: ActivatedRoute, private http: HttpClient) {}

  ngOnInit(): void {
    this.route.queryParamMap.pipe(debounceTime(100), take(1)).subscribe(params => {
      const token = params.get('token');
      const signature = params.get('signature');
      const packageId = params.get('packageId');
      const fileIds = params.get('fileIds');

      if (token && signature && packageId && fileIds) {
        this.getConfig().subscribe(config => {
          this.loadNewRelic(config);

          const apiHost = config.HOST_API_META;

          window.sessionStorage.setItem('token', token);

          this.http
            .get(`${apiHost}/packages/${packageId}/playlist/as?file-ids[]=${fileIds}&flash=0&signature=${signature}`)
            .pipe(catchError(res => {
              this.message.next(res?.error?.errors[0].message || 'Request failed');
              return of(null);
            }))
            .subscribe(res => {
              if (res) {
                debugger;
              }
            });
        });
      } else {
        this.message.next('URL parameters are missed (token, signature, packageId, fileIds)');
      }
    });
  }

  private getConfig(): Observable<Params> {
    return this.http.get('/config/config.json');
  }

  private loadNewRelic(data: Params) {
    var config = data['NEWRELIC'];
  
    if (!config) {
      return;
    }
  
    if (!(config['ACCOUNT_ID'] && config['AGENT_ID'] && config['APPLICATION_ID'] && config['LICENSE'])) {
      console.error('[ConfigService] New Relic configuration is not valid');
      return;
    }
  
    window['NREUM'].loader_config = {
      accountID: config['ACCOUNT_ID'],
      trustKey: config['ACCOUNT_ID'],
      agentID: config['AGENT_ID'],
      licenseKey: config['LICENSE'],
      applicationID: config['APPLICATION_ID']
    };
  
    window['NREUM'].info = {
      beacon: 'bam.nr-data.net',
      errorBeacon: 'bam.nr-data.net',
      licenseKey: config['LICENSE'],
      applicationID: config['APPLICATION_ID'],
      sa: 1
    };
  }
}
